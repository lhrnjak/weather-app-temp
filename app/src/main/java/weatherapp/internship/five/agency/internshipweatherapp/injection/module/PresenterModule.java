package weatherapp.internship.five.agency.internshipweatherapp.injection.module;

import dagger.Module;
import dagger.Provides;
import weatherapp.internship.five.agency.internshipweatherapp.injection.component.ActivityComponent;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.presenter.AdditionalInfoPresenter;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.presenter.AdditionalInfoPresenterImpl;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.viewmodel.converter.AdditionalInfoViewModelConverter;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.viewmodel.converter.AdditionalInfoViewModelConverterImpl;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.activity.BaseActivity;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.presenter.WeatherOverviewPresenter;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.presenter.WeatherOverviewPresenterImpl;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.viewmodel.converter.WeatherOverviewViewModelConverter;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.viewmodel.converter.WeatherOverviewViewModelConverterImpl;
import weatherapp.internship.five.agency.internshipweatherapp.util.ImageUrlBuilder;

@Module
public final class PresenterModule {

    private final BaseActivity baseActivity;

    public PresenterModule(final BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    private ActivityComponent getActivityComponent() {
        return baseActivity.getActivityComponent();
    }

    @Provides
    WeatherOverviewPresenter provideWeatherOverviewPresenter() {
        WeatherOverviewPresenter presenter = new WeatherOverviewPresenterImpl();
        getActivityComponent().inject(presenter);
        return presenter;
    }

    @Provides
    WeatherOverviewViewModelConverter provideWeatherOverviewViewModelConverter(final ImageUrlBuilder imageUrlBuilder) {
        return new WeatherOverviewViewModelConverterImpl(imageUrlBuilder);
    }

    @Provides
    AdditionalInfoPresenter provideAdditionalInfoPresenter() {
        AdditionalInfoPresenter presenter = new AdditionalInfoPresenterImpl();
        getActivityComponent().inject(presenter);
        return presenter;
    }

    @Provides
    AdditionalInfoViewModelConverter provideAdditionalInfoViewModelConverter(final ImageUrlBuilder imageUrlBuilder) {
        return new AdditionalInfoViewModelConverterImpl(imageUrlBuilder);
    }

    public interface Exposes {

        WeatherOverviewPresenter weatherOverviewPresenter();

        WeatherOverviewViewModelConverter weatherOverviewViewModelConverter();

        AdditionalInfoPresenter additionalInfoPresenter();

        AdditionalInfoViewModelConverter additionalInfoViewModelConverter();
    }
}
