package weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.viewmodel;

public final class AdditionalInfoViewModel {

    public final String cityName;
    public final String weatherDescription;
    public final String weatherConditionIconUrl;
    public final double currentTemperature;
    public final double minimumTemperature;
    public final double maximumTemperature;
    public final int pressure;
    public final int humidity;
    public final int visibility;
    public final double windSpeed;
    public final int direction;
    public final int cloudCover;

    public AdditionalInfoViewModel(final String cityName, final String weatherDescription, final String weatherConditionIconUrl, final double currentTemperature,
                                   final double minimumTemperature,
                                   final double maximumTemperature, final int pressure, final int humidity, final int visibility, final double windSpeed, final int direction,
                                   final int cloudCover) {
        this.cityName = cityName;
        this.weatherDescription = weatherDescription;
        this.weatherConditionIconUrl = weatherConditionIconUrl;
        this.currentTemperature = currentTemperature;
        this.minimumTemperature = minimumTemperature;
        this.maximumTemperature = maximumTemperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.visibility = visibility;
        this.windSpeed = windSpeed;
        this.direction = direction;
        this.cloudCover = cloudCover;
    }
}
