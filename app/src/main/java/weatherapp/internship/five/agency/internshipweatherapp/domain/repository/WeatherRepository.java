package weatherapp.internship.five.agency.internshipweatherapp.domain.repository;

import io.reactivex.Observable;
import weatherapp.internship.five.agency.internshipweatherapp.domain.model.Weather;

public interface WeatherRepository {

    Observable<Weather> getWeatherForCity(String city);
}
