package weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.viewmodel;

public final class WeatherOverviewViewModel {

    public static final WeatherOverviewViewModel EMPTY = new WeatherOverviewViewModel("", "", "", 0, 0, 0);

    public final String cityName;
    public final String weatherDescription;
    public final String weatherConditionIconUrl;
    public final double currentTemperature;
    public final double minimumTemperature;
    public final double maximumTemperature;

    public WeatherOverviewViewModel(final String cityName, final String weatherDescription, final String weatherConditionIconUrl, final double currentTemperature,
                                    final double minimumTemperature,
                                    final double maximumTemperature) {
        this.cityName = cityName;
        this.weatherDescription = weatherDescription;
        this.weatherConditionIconUrl = weatherConditionIconUrl;
        this.currentTemperature = currentTemperature;
        this.minimumTemperature = minimumTemperature;
        this.maximumTemperature = maximumTemperature;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final WeatherOverviewViewModel that = (WeatherOverviewViewModel) o;

        if (Double.compare(that.currentTemperature, currentTemperature) != 0) {
            return false;
        }
        if (Double.compare(that.minimumTemperature, minimumTemperature) != 0) {
            return false;
        }
        if (Double.compare(that.maximumTemperature, maximumTemperature) != 0) {
            return false;
        }
        if (cityName != null ? !cityName.equals(that.cityName) : that.cityName != null) {
            return false;
        }
        if (weatherDescription != null ? !weatherDescription.equals(that.weatherDescription) : that.weatherDescription != null) {
            return false;
        }
        return weatherConditionIconUrl != null ? weatherConditionIconUrl.equals(that.weatherConditionIconUrl) : that.weatherConditionIconUrl == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = cityName != null ? cityName.hashCode() : 0;
        result = 31 * result + (weatherDescription != null ? weatherDescription.hashCode() : 0);
        result = 31 * result + (weatherConditionIconUrl != null ? weatherConditionIconUrl.hashCode() : 0);
        temp = Double.doubleToLongBits(currentTemperature);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(minimumTemperature);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(maximumTemperature);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "WeatherOverviewViewModel{" +
                "cityName='" + cityName + '\'' +
                ", weatherDescription='" + weatherDescription + '\'' +
                ", weatherConditionIconId='" + weatherConditionIconUrl + '\'' +
                ", currentTemperature=" + currentTemperature +
                ", minimumTemperature=" + minimumTemperature +
                ", maximumTemperature=" + maximumTemperature +
                '}';
    }
}
