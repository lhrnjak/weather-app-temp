package weatherapp.internship.five.agency.internshipweatherapp.injection.module;

import android.os.Handler;
import android.os.Looper;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Module
public final class ThreadingModule {

    public static final String UI_HANDLER = "ui_handler";
    public static final String OBSERVE_SCHEDULER = "observe_scheduler";
    public static final String SUBSCRIBE_SCHEDULER = "subscribe_scheduler";

    @Provides
    @Singleton
    @Named(UI_HANDLER)
    public Handler provideHandler() {
        return new Handler(Looper.getMainLooper());
    }

    @Provides
    @Singleton
    @Named(ThreadingModule.OBSERVE_SCHEDULER)
    public Scheduler provideObserveOnScheduler() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @Singleton
    @Named(ThreadingModule.SUBSCRIBE_SCHEDULER)
    public Scheduler provideSubscribeOnScheduler() {
        return Schedulers.io();
    }

    public interface Exposes {

        @Named(UI_HANDLER)
        Handler handler();

        @Named(ThreadingModule.OBSERVE_SCHEDULER)
        Scheduler observeScheduler();

        @Named(ThreadingModule.SUBSCRIBE_SCHEDULER)
        Scheduler subscribeScheduler();
    }
}
