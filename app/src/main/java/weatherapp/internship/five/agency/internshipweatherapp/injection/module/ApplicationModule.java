package weatherapp.internship.five.agency.internshipweatherapp.injection.module;

import android.content.Context;
import android.content.res.Resources;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import weatherapp.internship.five.agency.internshipweatherapp.application.WeatherApplication;
import weatherapp.internship.five.agency.internshipweatherapp.injection.qualifier.ForApplication;

@Module
public final class ApplicationModule {

    private final WeatherApplication weatherApplication;

    public ApplicationModule(final WeatherApplication weatherApplication) {
        this.weatherApplication = weatherApplication;
    }

    @Provides
    @ForApplication
    public Context provideApplicationContext() {
        return weatherApplication;
    }

    @Provides
    @Singleton
    public Resources provideResources() {
        return weatherApplication.getResources();
    }

    public interface Exposes {

        @ForApplication
        Context context();

        Resources resources();
    }
}
