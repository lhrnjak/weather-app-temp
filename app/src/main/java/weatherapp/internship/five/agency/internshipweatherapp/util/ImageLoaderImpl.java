package weatherapp.internship.five.agency.internshipweatherapp.util;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class ImageLoaderImpl implements ImageLoader {

    private final Picasso picasso;
    private final UriUtils uriUtils;

    public ImageLoaderImpl(final Picasso picasso, final UriUtils uriUtils) {
        this.picasso = picasso;
        this.uriUtils = uriUtils;
    }

    @Override
    public void loadImage(final String url, final ImageView target) {
        picasso.load(uriUtils.toUriOrDefault(url)).into(target);
    }

    @Override
    public void loadImageResize(final String url, final ImageView target, final int width, final int height) {
        picasso.load(uriUtils.toUriOrDefault(url)).resize(width, height).into(target);
    }
}
