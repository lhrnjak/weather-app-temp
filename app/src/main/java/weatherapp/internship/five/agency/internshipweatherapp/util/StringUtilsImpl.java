package weatherapp.internship.five.agency.internshipweatherapp.util;

import android.support.annotation.NonNull;

public class StringUtilsImpl implements StringUtils {

    @Override
    public String itOrDefault(final String text, @NonNull final String defaultText) {
        if (defaultText == null) {
            throw new NullPointerException("defaultText = null");
        }
        return isEmpty(text) ? defaultText : text;
    }

    @Override
    public boolean isEmpty(final CharSequence text) {
        return text == null || text.length() == 0;
    }
}
