package weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.presenter;

import javax.inject.Inject;

import io.reactivex.Observable;
import weatherapp.internship.five.agency.internshipweatherapp.domain.model.Weather;
import weatherapp.internship.five.agency.internshipweatherapp.domain.usecase.GetWeatherForCityUseCase;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.view.AdditionalInfoView;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.viewmodel.AdditionalInfoViewModel;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.viewmodel.converter.AdditionalInfoViewModelConverter;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.presenter.BasePresenter;

public class AdditionalInfoPresenterImpl extends BasePresenter<AdditionalInfoView> implements AdditionalInfoPresenter {

    private static final int GET_WEATHER_RETRY_COUNT = 3;

    @Inject
    GetWeatherForCityUseCase getWeatherForCityUseCase;

    @Inject
    AdditionalInfoViewModelConverter additionalInfoViewModelConverter;

    private String cityName = "";

    public AdditionalInfoPresenterImpl() {
        super(AdditionalInfoView.EMPTY);
    }

    public void setCityName(final String cityName) {
        this.cityName = cityName;
    }

    @Override
    public void setView(final AdditionalInfoView view) {
        setViewInternal(view);
    }

    @Override
    public void activate() {
        super.activate();
        fetchData();
    }

    @Override
    public void deactivate() {
        super.deactivate();
    }

    private void fetchData() {
        addDisposable(Observable.just(cityName)
                                .flatMap(getWeatherForCityUseCase::execute)
                                .retry(GET_WEATHER_RETRY_COUNT)
                                .observeOn(observeScheduler)
                                .subscribeOn(subscribeScheduler)
                                .subscribe(this::processGetWeatherSuccess,
                                           this::processGetWeatherFailure));
    }

    private void processGetWeatherSuccess(final Weather weather) {
        renderView(additionalInfoViewModelConverter.convertToAdditionalInfoViewModel(weather));
    }

    private void processGetWeatherFailure(final Throwable throwable) {

    }

    private void renderView(AdditionalInfoViewModel viewModel) {
        getView().renderView(viewModel);
    }
}
