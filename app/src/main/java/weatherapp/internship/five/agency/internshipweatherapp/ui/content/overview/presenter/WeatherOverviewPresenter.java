package weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.presenter;

import weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.Scoped;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.view.WeatherOverviewView;

public interface WeatherOverviewPresenter extends Scoped {

    void setView(WeatherOverviewView view);

    void showAdditionalInfo(String cityName);
}
