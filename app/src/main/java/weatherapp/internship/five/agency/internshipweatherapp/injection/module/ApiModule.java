package weatherapp.internship.five.agency.internshipweatherapp.injection.module;

import android.content.res.Resources;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import weatherapp.internship.five.agency.internshipweatherapp.R;
import weatherapp.internship.five.agency.internshipweatherapp.data.api.WeatherRetrofitApi;
import weatherapp.internship.five.agency.internshipweatherapp.data.api.client.WeatherRetrofitClient;
import weatherapp.internship.five.agency.internshipweatherapp.data.api.client.WeatherRetrofitClientImpl;
import weatherapp.internship.five.agency.internshipweatherapp.data.api.converter.ApiConverter;
import weatherapp.internship.five.agency.internshipweatherapp.data.api.converter.ApiConverterImpl;
import weatherapp.internship.five.agency.internshipweatherapp.util.StringUtils;

@Module
public final class ApiModule {

    @Provides
    @Singleton
    public Retrofit provideRetrofit() {
        return new Retrofit.Builder().baseUrl(WeatherRetrofitApi.API_URL)
                                     .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                                     .addConverterFactory(GsonConverterFactory.create())
                                     .build();
    }

    @Provides
    @Singleton
    public WeatherRetrofitApi provideWeatherRetrofitApi(final Retrofit retrofit) {
        return retrofit.create(WeatherRetrofitApi.class);
    }

    @Provides
    @Singleton
    public WeatherRetrofitClient provideWeatherRetrofitClient(final ApiConverter apiConverter, final WeatherRetrofitApi weatherRetrofitApi, final Resources resources) {
        final String apiKey = resources.getString(R.string.open_weather_map_api_key);
        return new WeatherRetrofitClientImpl(apiConverter, weatherRetrofitApi, apiKey);
    }

    @Provides
    @Singleton
    public ApiConverter provideApiConverter(final StringUtils stringUtils) {
        return new ApiConverterImpl(stringUtils);
    }

    public interface Exposes {

        Retrofit retrofit();

        WeatherRetrofitApi weatherRetrofitApi();

        WeatherRetrofitClient weatherRetrofitClient();

        ApiConverter apiConverter();
    }
}
