package weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.presenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import weatherapp.internship.five.agency.internshipweatherapp.domain.model.Weather;
import weatherapp.internship.five.agency.internshipweatherapp.domain.usecase.GetWeatherForCityUseCase;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.presenter.BasePresenter;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.router.WeatherOverviewRouter;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.view.WeatherOverviewView;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.viewmodel.WeatherOverviewScreenViewModel;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.viewmodel.WeatherOverviewViewModel;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.viewmodel.converter.WeatherOverviewViewModelConverter;

public class WeatherOverviewPresenterImpl extends BasePresenter<WeatherOverviewView> implements WeatherOverviewPresenter {

    private static final int GET_WEATHER_RETRY_COUNT = 3;

    @Inject
    GetWeatherForCityUseCase getWeatherForCityUseCase;

    @Inject
    WeatherOverviewViewModelConverter weatherOverviewViewModelConverter;

    private final List<WeatherOverviewViewModel> weatherData = new ArrayList<>();

    private final String[] cities = {"Zagreb", "Split", "Rijeka", "Osijek"};

    @Inject
    WeatherOverviewRouter router;

    public WeatherOverviewPresenterImpl() {
        super(WeatherOverviewView.EMPTY);
    }

    @Override
    public void setView(final WeatherOverviewView view) {
        setViewInternal(view);
    }

    @Override
    public void showAdditionalInfo(String cityName) {
        router.showAdditionalInfoScreen(cityName);
    }

    @Override
    public void activate() {
        super.activate();
        fetchWeatherData();
    }

    @Override
    public void deactivate() {
        super.deactivate();
    }

    private void fetchWeatherData() {
        addDisposable(Observable.fromArray(cities)
                                .flatMap(getWeatherForCityUseCase::execute)
                                .toList()
                                .retry(GET_WEATHER_RETRY_COUNT)
                                .observeOn(observeScheduler)
                                .subscribeOn(subscribeScheduler)
                                .subscribe(this::processGetWeatherSuccess,
                                           this::processGetWeatherFailure));
    }

    private void processGetWeatherSuccess(final List<Weather> weathers) {
        weatherData.clear();
        for (final Weather weather : weathers) {
            weatherData.add(weatherOverviewViewModelConverter.convertToViewModel(weather));
        }
        renderView(new WeatherOverviewScreenViewModel(weatherData));
    }

    private void processGetWeatherFailure(final Throwable throwable) {

    }

    private void renderView(final WeatherOverviewScreenViewModel viewModel) {
        getView().renderView(viewModel);
    }
}
