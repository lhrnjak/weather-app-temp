package weatherapp.internship.five.agency.internshipweatherapp.data.api.client;

import io.reactivex.Single;
import weatherapp.internship.five.agency.internshipweatherapp.domain.model.Weather;

public interface WeatherRetrofitClient {

    Single<Weather> getWeather(final String query, final String units);
}
