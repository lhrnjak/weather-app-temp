package weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.viewmodel.converter;

import weatherapp.internship.five.agency.internshipweatherapp.domain.model.Weather;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.viewmodel.AdditionalInfoViewModel;

public interface AdditionalInfoViewModelConverter {

    AdditionalInfoViewModel convertToAdditionalInfoViewModel(Weather weather);
}
