package weatherapp.internship.five.agency.internshipweatherapp.domain.usecase;

import io.reactivex.Observable;
import weatherapp.internship.five.agency.internshipweatherapp.domain.model.Weather;

public interface GetWeatherForCityUseCase {

    Observable<Weather> execute(String city);
}
