package weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.view;

import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.viewmodel.AdditionalInfoViewModel;

public final class EmptyAdditionalInfoView implements AdditionalInfoView {

    @Override
    public void renderView(final AdditionalInfoViewModel viewModel) {
        // NO OP
    }
}
