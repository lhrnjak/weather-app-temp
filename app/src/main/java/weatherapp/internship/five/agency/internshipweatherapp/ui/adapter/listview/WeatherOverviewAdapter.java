package weatherapp.internship.five.agency.internshipweatherapp.ui.adapter.listview;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import weatherapp.internship.five.agency.internshipweatherapp.R;
import weatherapp.internship.five.agency.internshipweatherapp.ui.adapter.listview.callback.WeatherOverviewDiffCallback;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.viewmodel.WeatherOverviewViewModel;
import weatherapp.internship.five.agency.internshipweatherapp.util.ImageLoader;
import weatherapp.internship.five.agency.internshipweatherapp.util.ObjectUtils;

public class WeatherOverviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface Listener {

        Listener EMPTY = new EmptyListener();

        void citySelected(String cityName);
    }

    private static final int VIEW_TYPE_WEATHER = 1;

    private final LayoutInflater layoutInflater;
    private final Resources resources;
    private final ImageLoader imageLoader;

    private final List<WeatherOverviewViewModel> weatherData = new ArrayList<>();

    private Listener listener = Listener.EMPTY;

    public WeatherOverviewAdapter(final LayoutInflater layoutInflater, final Resources resources, final ImageLoader imageLoader) {
        this.layoutInflater = layoutInflater;
        this.resources = resources;
        this.imageLoader = imageLoader;
    }

    public void setWeatherData(@NonNull final List<WeatherOverviewViewModel> weatherData) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new WeatherOverviewDiffCallback(this.weatherData, weatherData));
        this.weatherData.clear();
        this.weatherData.addAll(weatherData);
        diffResult.dispatchUpdatesTo(this);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        switch (viewType) {
            case VIEW_TYPE_WEATHER:
                return new WeatherOverviewViewHolder(layoutInflater.inflate(R.layout.weather_overview_adapter_item, parent, false));
            default:
                throw new IllegalArgumentException("Unknown view type: " + viewType);
        }
    }

    @Override
    public int getItemViewType(final int position) {
        return VIEW_TYPE_WEATHER;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final int viewType = getItemViewType(position);
        if (viewType == VIEW_TYPE_WEATHER) {
            final WeatherOverviewViewModel data = weatherData.get(position);
            final WeatherOverviewViewHolder weatherOverviewViewHolder = (WeatherOverviewViewHolder) holder;

            weatherOverviewViewHolder.listener = listener;

            weatherOverviewViewHolder.cityName = data.cityName;

            fillViews(weatherOverviewViewHolder, data);
        } else {

        }
    }

    private void fillViews(final WeatherOverviewViewHolder holder, final WeatherOverviewViewModel data) {
        final String weatherDescription = Character.toUpperCase(data.weatherDescription.charAt(0)) + data.weatherDescription.substring(1);

        holder.cityNameTextView.setText(data.cityName);
        holder.weatherDescriptionTextView.setText(weatherDescription);
        holder.currentTemperatureTextView.setText(String.format(Locale.getDefault(), resources.getString(R.string.string_format_current_temperature), data.currentTemperature));
        holder.minimumTemperatureTextView.setText(String.format(Locale.getDefault(), resources.getString(R.string.string_format_min_temperature), data.minimumTemperature));
        holder.maximumTemperatureTextView.setText(String.format(Locale.getDefault(), resources.getString(R.string.string_format_max_temperature), data.maximumTemperature));

        imageLoader.loadImage(data.weatherConditionIconUrl, holder.weatherConditionImageView);
    }

    @Override
    public int getItemCount() {
        return weatherData.size();
    }

    public void setListener(final Listener listener) {
        this.listener = ObjectUtils.itOrDefault(listener, Listener.EMPTY);
        notifyDataSetChanged();
    }

    static final class WeatherOverviewViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_weather_overview_city_name_text)
        TextView cityNameTextView;

        @BindView(R.id.adapter_weather_overview_weather_description_text)
        TextView weatherDescriptionTextView;

        @BindView(R.id.adapter_weather_overview_current_temperature_text)
        TextView currentTemperatureTextView;

        @BindView(R.id.adapter_weather_overview_minimum_temperature_text)
        TextView minimumTemperatureTextView;

        @BindView(R.id.adapter_weather_overview_maximum_temperature_text)
        TextView maximumTemperatureTextView;

        @BindView(R.id.adapter_weather_overview_weather_condition_image)
        ImageView weatherConditionImageView;

        private String cityName = "";

        private Listener listener = Listener.EMPTY;

        WeatherOverviewViewHolder(View rootView) {
            super(rootView);
            bindViews(rootView);
        }

        private void bindViews(final View rootView) {
            ButterKnife.bind(this, rootView);
        }

        @OnClick(R.id.adapter_weather_overview_linear_layout)
        void onRootClick() {
            listener.citySelected(cityName);
        }
    }

    private static final class EmptyListener implements Listener {

        @Override
        public void citySelected(final String cityName) {

        }
    }
}
