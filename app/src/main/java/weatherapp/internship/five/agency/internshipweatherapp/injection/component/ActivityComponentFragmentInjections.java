package weatherapp.internship.five.agency.internshipweatherapp.injection.component;

import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.fragments.AdditionalInfoFragment;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.presenter.AdditionalInfoPresenter;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.fragments.WeatherOverviewFragment;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.presenter.WeatherOverviewPresenter;

public interface ActivityComponentFragmentInjections {

    void inject(WeatherOverviewFragment fragment);
    void inject(WeatherOverviewPresenter presenter);

    void inject(AdditionalInfoFragment fragment);
    void inject(AdditionalInfoPresenter presenter);
}
