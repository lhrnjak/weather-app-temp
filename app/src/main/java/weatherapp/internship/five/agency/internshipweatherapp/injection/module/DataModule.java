package weatherapp.internship.five.agency.internshipweatherapp.injection.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import weatherapp.internship.five.agency.internshipweatherapp.data.api.client.WeatherRetrofitClient;
import weatherapp.internship.five.agency.internshipweatherapp.domain.repository.WeatherRepository;
import weatherapp.internship.five.agency.internshipweatherapp.domain.repository.WeatherRepositoryImpl;

@Module
public final class DataModule {

    @Provides
    @Singleton
    public WeatherRepository provideWeatherRepository(final WeatherRetrofitClient weatherRetrofitClient) {
        return new WeatherRepositoryImpl(weatherRetrofitClient);
    }

    public interface Exposes {

        WeatherRepository weatherRepository();
    }
}
