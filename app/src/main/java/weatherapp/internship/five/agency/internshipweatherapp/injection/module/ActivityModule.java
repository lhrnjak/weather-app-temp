package weatherapp.internship.five.agency.internshipweatherapp.injection.module;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;

import dagger.Module;
import dagger.Provides;
import weatherapp.internship.five.agency.internshipweatherapp.injection.qualifier.ForActivity;
import weatherapp.internship.five.agency.internshipweatherapp.injection.scope.ActivityScope;
import weatherapp.internship.five.agency.internshipweatherapp.ui.adapter.listview.WeatherOverviewAdapter;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.activity.BaseActivity;
import weatherapp.internship.five.agency.internshipweatherapp.util.ImageLoader;

@Module
public final class ActivityModule {

    private final BaseActivity baseActivity;

    public ActivityModule(final BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    @Provides
    @ActivityScope
    public Activity provideActivity() {
        return baseActivity;
    }

    @Provides
    @ActivityScope
    public BaseActivity provideBaseActivity() {
        return baseActivity;
    }

    @Provides
    @ForActivity
    @ActivityScope
    public Context provideContext() {
        return baseActivity;
    }

    @Provides
    @ActivityScope
    public LayoutInflater layoutInflater(@ForActivity final Context context) {
        return LayoutInflater.from(context);
    }

    @Provides
    @ActivityScope
    public FragmentManager provideFragmentManager() {
        return baseActivity.getSupportFragmentManager();
    }

    @Provides
    public WeatherOverviewAdapter provideWeatherOverviewAdapter(final LayoutInflater layoutInflater, final Resources resources, final ImageLoader imageLoader) {
        return new WeatherOverviewAdapter(layoutInflater, resources, imageLoader);
    }

    public interface Exposes {

        @ForActivity
        Context context();
    }
}
