package weatherapp.internship.five.agency.internshipweatherapp.util;

import android.net.Uri;

public class UriUtilsImpl implements UriUtils {

    private final StringUtils stringUtils;

    public UriUtilsImpl(final StringUtils stringUtils) {
        this.stringUtils = stringUtils;
    }

    @Override
    public Uri toUriOrDefault(final String uriAsString) {
        return stringUtils.isEmpty(uriAsString) ? Uri.EMPTY : Uri.parse(uriAsString);
    }
}
