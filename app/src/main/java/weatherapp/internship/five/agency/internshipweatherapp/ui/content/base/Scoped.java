package weatherapp.internship.five.agency.internshipweatherapp.ui.content.base;

public interface Scoped {

    void activate();

    void deactivate();
}
