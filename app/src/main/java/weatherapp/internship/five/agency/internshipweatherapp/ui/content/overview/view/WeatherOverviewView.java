package weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.view;

import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.viewmodel.WeatherOverviewScreenViewModel;

public interface WeatherOverviewView {

    WeatherOverviewView EMPTY = new EmptyWeatherOverviewView();

    void renderView(WeatherOverviewScreenViewModel viewModel);
}
