package weatherapp.internship.five.agency.internshipweatherapp.ui.content.launcher.activity;

import android.os.Bundle;

import butterknife.ButterKnife;
import weatherapp.internship.five.agency.internshipweatherapp.R;
import weatherapp.internship.five.agency.internshipweatherapp.injection.component.ActivityComponent;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.activity.BaseActivity;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.activity.WeatherOverviewActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();

        startActivity(WeatherOverviewActivity.createIntent(this));
        finish();
    }

    @Override
    protected void inject(final ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    private void bindViews() {
        ButterKnife.bind(this);
    }
}
