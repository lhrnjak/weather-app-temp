package weatherapp.internship.five.agency.internshipweatherapp.injection.component;

import weatherapp.internship.five.agency.internshipweatherapp.injection.module.ActivityModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.PresenterModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.RouterModule;

public interface ActivityComponentExposes extends ActivityModule.Exposes,
                                                  PresenterModule.Exposes,
                                                  RouterModule.Exposes {

}
