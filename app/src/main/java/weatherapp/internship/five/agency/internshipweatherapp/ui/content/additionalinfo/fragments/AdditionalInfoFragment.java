package weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import weatherapp.internship.five.agency.internshipweatherapp.R;
import weatherapp.internship.five.agency.internshipweatherapp.injection.component.ActivityComponent;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.presenter.AdditionalInfoPresenter;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.view.AdditionalInfoView;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.viewmodel.AdditionalInfoViewModel;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.fragments.BaseFragment;
import weatherapp.internship.five.agency.internshipweatherapp.util.ImageLoader;

public class AdditionalInfoFragment extends BaseFragment implements AdditionalInfoView {

    private static final String CITY_NAME = "city_name_for_query";

    private static final int WIND_DIRECTION_DEFAULT_ROTATION = 90;

    @BindView(R.id.fragment_additional_info_city_name_text)
    protected TextView cityNameTextView;

    @BindView(R.id.fragment_additional_info_weather_description_text)
    protected TextView weatherDescriptionTextView;

    @BindView(R.id.fragment_additional_info_current_temp_text)
    protected TextView currentTemperatureTextView;

    @BindView(R.id.fragment_additional_info_min_temp_text)
    protected TextView minimumTemperatureTextView;

    @BindView(R.id.fragment_additional_info_max_temp_text)
    protected TextView maximumTemperatureTextView;

    @BindView(R.id.fragment_additional_info_pressure_text)
    protected TextView pressureTextView;

    @BindView(R.id.fragment_additional_info_humidity_text)
    protected TextView humidityTextView;

    @BindView(R.id.fragment_additional_info_visibility_text)
    protected TextView visibilityTextView;

    @BindView(R.id.fragment_additional_info_wind_speed_text)
    protected TextView windSpeedTextView;

    @BindView(R.id.fragment_additional_info_wind_direction_text)
    protected TextView windDirectionTextView;

    @BindView(R.id.fragment_additional_info_cloud_cover_text)
    protected TextView cloudCoverTextView;

    @BindView(R.id.fragment_additional_info_wind_direction_image)
    protected ImageView windDirectionImageView;

    @BindView(R.id.fragment_additional_info_weather_condition_image)
    protected ImageView weatherConditionImageView;

    @Inject
    AdditionalInfoPresenter presenter;

    @Inject
    ImageLoader imageLoader;

    public static Fragment newInstance(final String cityName) {
        final Bundle arguments = new Bundle();
        arguments.putString(CITY_NAME, cityName);

        final AdditionalInfoFragment fragment = new AdditionalInfoFragment();
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Bundle arguments = getArguments();
        if (arguments == null) {
            throw new IllegalStateException("Fragment must be instantiated with arguments");
        }

        if (!arguments.containsKey(CITY_NAME)) {
            throw new IllegalArgumentException("Bundle must contain city name");
        }

        final String cityName = arguments.getString(CITY_NAME);
        presenter.setCityName(cityName);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        return bindViews(inflater.inflate(R.layout.fragment_additional_info, container, false));
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void renderView(final AdditionalInfoViewModel viewModel) {
        fillViews(viewModel);
    }

    @Override
    public void onPause() {
        presenter.deactivate();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.setView(this);
        presenter.activate();
    }

    @Override
    public void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    private void fillViews(AdditionalInfoViewModel viewModel) {
        final String weatherDescription = Character.toUpperCase(viewModel.weatherDescription.charAt(0)) + viewModel.weatherDescription.substring(1);

        cityNameTextView.setText(viewModel.cityName);
        weatherDescriptionTextView.setText(weatherDescription);

        currentTemperatureTextView.setText(formatString(R.string.string_format_current_temperature, viewModel.currentTemperature));
        minimumTemperatureTextView.setText(formatString(R.string.string_format_min_temperature, viewModel.minimumTemperature));
        maximumTemperatureTextView.setText(formatString(R.string.string_format_max_temperature, viewModel.maximumTemperature));
        pressureTextView.setText(formatString(R.string.string_format_pressure, viewModel.pressure));
        humidityTextView.setText(formatString(R.string.string_format_humidity, viewModel.humidity));
        visibilityTextView.setText(formatString(R.string.string_format_visibility, viewModel.visibility));
        windSpeedTextView.setText(formatString(R.string.string_format_wind_speed, viewModel.windSpeed));
        windDirectionTextView.setText(formatString(R.string.string_format_wind_direction, viewModel.direction));
        cloudCoverTextView.setText(formatString(R.string.string_format_cloud_cover, viewModel.cloudCover));

        windDirectionImageView.setVisibility(View.VISIBLE);
        windDirectionImageView.setRotation(viewModel.direction + WIND_DIRECTION_DEFAULT_ROTATION);

        imageLoader.loadImage(viewModel.weatherConditionIconUrl, weatherConditionImageView);
    }

    private String formatString(int resourceId, int value) {
        return String.format(Locale.getDefault(), resources.getString(resourceId), value);
    }

    private String formatString(int resourceId, double value) {
        return String.format(Locale.getDefault(), resources.getString(resourceId), value);
    }
}
