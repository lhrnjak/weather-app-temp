package weatherapp.internship.five.agency.internshipweatherapp.util;

import android.support.annotation.NonNull;

import java.util.Objects;

public class ObjectUtils {

    private ObjectUtils() {

    }

    public static <T> T itOrDefault(final T value, @NonNull final T defaultValue) {
        Objects.requireNonNull(defaultValue, "Default value must not be null");

        return value == null ? defaultValue : value;
    }
}
