package weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.router;

public interface WeatherOverviewRouter {

    void showWeatherOverviewScreen();

    void showAdditionalInfoScreen(String cityName);
}