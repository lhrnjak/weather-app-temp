package weatherapp.internship.five.agency.internshipweatherapp.injection.component;

import weatherapp.internship.five.agency.internshipweatherapp.application.WeatherApplication;

public interface ApplicationComponentInjects {

    void inject(WeatherApplication app);
}
