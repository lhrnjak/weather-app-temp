package weatherapp.internship.five.agency.internshipweatherapp.application;

import android.app.Application;
import android.content.Context;

import weatherapp.internship.five.agency.internshipweatherapp.injection.ComponentFactory;
import weatherapp.internship.five.agency.internshipweatherapp.injection.component.ApplicationComponent;

public class WeatherApplication extends Application {

    private ApplicationComponent applicationComponent;

    public static WeatherApplication from(final Context context) {
        return (WeatherApplication) context.getApplicationContext();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        injectMe();
    }

    private void injectMe() {
        applicationComponent = ComponentFactory.createApplicationComponent(this);
        applicationComponent.inject(this);
    }
}
