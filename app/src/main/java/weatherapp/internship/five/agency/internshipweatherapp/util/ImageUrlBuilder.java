package weatherapp.internship.five.agency.internshipweatherapp.util;

public interface ImageUrlBuilder {

    String OPEN_WEATHER_MAP_ICON_BASIC_URL = "http://openweathermap.org/img/w/";

    String generateWeatherIconUrl(String iconId);
}
