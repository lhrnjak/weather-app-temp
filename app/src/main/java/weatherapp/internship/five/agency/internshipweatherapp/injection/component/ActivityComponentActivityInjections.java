package weatherapp.internship.five.agency.internshipweatherapp.injection.component;

import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.activity.AdditionalInfoActivity;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.launcher.activity.MainActivity;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.activity.WeatherOverviewActivity;

public interface ActivityComponentActivityInjections {

    void inject(MainActivity activity);

    void inject(WeatherOverviewActivity activity);

    void inject(AdditionalInfoActivity activity);
}
