package weatherapp.internship.five.agency.internshipweatherapp.data.api.converter;

import weatherapp.internship.five.agency.internshipweatherapp.data.api.models.ApiWeatherResponse;
import weatherapp.internship.five.agency.internshipweatherapp.domain.model.Weather;

public interface ApiConverter {

    Weather convertToWeather(ApiWeatherResponse apiWeatherResponse);
}
