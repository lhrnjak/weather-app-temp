package weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.viewmodel.converter;

import weatherapp.internship.five.agency.internshipweatherapp.domain.model.Weather;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.viewmodel.WeatherOverviewViewModel;
import weatherapp.internship.five.agency.internshipweatherapp.util.ImageUrlBuilder;

public class WeatherOverviewViewModelConverterImpl implements WeatherOverviewViewModelConverter {

    private final ImageUrlBuilder imageUrlBuilder;

    public WeatherOverviewViewModelConverterImpl(final ImageUrlBuilder imageUrlBuilder) {
        this.imageUrlBuilder = imageUrlBuilder;
    }

    @Override
    public WeatherOverviewViewModel convertToViewModel(final Weather weather) {
        final String iconUrl = imageUrlBuilder.generateWeatherIconUrl(weather.weatherCondition.get(0).icon);

        return new WeatherOverviewViewModel(weather.city, weather.weatherCondition.get(0).description,
                                            iconUrl, weather.temperature,
                                            weather.minimumTemperature, weather.maximumTemperature);
    }
}
