package weatherapp.internship.five.agency.internshipweatherapp.util;

import android.widget.ImageView;

public interface ImageLoader {

    void loadImage(final String url, final ImageView target);

    void loadImageResize(final String url, final ImageView target, final int width, final int height);
}
