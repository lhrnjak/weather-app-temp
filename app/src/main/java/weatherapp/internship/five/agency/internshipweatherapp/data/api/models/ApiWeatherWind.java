package weatherapp.internship.five.agency.internshipweatherapp.data.api.models;

import com.google.gson.annotations.SerializedName;

public class ApiWeatherWind {

    @SerializedName("speed")
    public double speed;

    @SerializedName("deg")
    public int direction;
}
