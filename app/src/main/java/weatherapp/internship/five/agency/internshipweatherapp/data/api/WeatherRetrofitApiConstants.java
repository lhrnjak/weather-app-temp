package weatherapp.internship.five.agency.internshipweatherapp.data.api;

public class WeatherRetrofitApiConstants {

    /* PATHS */
    static final String PATH_CURRENT_WEATHER = "weather";

    /* PARAMS */
    static final String PARAM_QUERY = "q";
    static final String PARAM_UNITS = "units";
    static final String PARAM_APPID = "appid";
}
