package weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import javax.inject.Inject;

import butterknife.ButterKnife;
import weatherapp.internship.five.agency.internshipweatherapp.R;
import weatherapp.internship.five.agency.internshipweatherapp.injection.component.ActivityComponent;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.router.AdditionalInfoRouter;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.activity.BaseActivity;

public class AdditionalInfoActivity extends BaseActivity {

    public static Intent createIntent(final Context context) {
        return new Intent(context, AdditionalInfoActivity.class);
    }

    @Inject
    AdditionalInfoRouter router;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_info);

        bindViews();
        if (savedInstanceState == null) {
            router.showAdditionalInfoScreen(getIntent().getStringExtra(Intent.EXTRA_TEXT));
        }
    }

    private void bindViews() {
        ButterKnife.bind(this);
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }
}
