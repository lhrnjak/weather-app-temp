package weatherapp.internship.five.agency.internshipweatherapp.util;

import android.support.annotation.NonNull;

public interface StringUtils {

    String itOrDefault(String text, @NonNull String defaultText);

    boolean isEmpty(CharSequence text);
}
