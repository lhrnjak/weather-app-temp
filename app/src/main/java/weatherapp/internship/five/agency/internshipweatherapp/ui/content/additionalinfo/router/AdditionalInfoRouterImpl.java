package weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.router;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import weatherapp.internship.five.agency.internshipweatherapp.R;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.fragments.AdditionalInfoFragment;

public class AdditionalInfoRouterImpl implements AdditionalInfoRouter {

    private static final String FRAGMENT_ADDITIONAL_INFO_FRAGMENT_TAG = "additional_info_fragment";
    private static final int PRIMARY_FRAGMENT_CONTAINER = R.id.activity_additional_info_container;

    private final Activity activity;
    private final FragmentManager fragmentManager;

    public AdditionalInfoRouterImpl(final Activity activity, final FragmentManager fragmentManager) {
        this.activity = activity;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void showAdditionalInfoScreen(String cityName) {
        addFragmentToUi(AdditionalInfoFragment.newInstance(cityName), FRAGMENT_ADDITIONAL_INFO_FRAGMENT_TAG);
    }

    private void addFragmentToUi(final Fragment fragment, final String tag) {
        fragmentManager.beginTransaction()
                       .replace(PRIMARY_FRAGMENT_CONTAINER, fragment, tag)
                       .commitAllowingStateLoss();
    }
}
