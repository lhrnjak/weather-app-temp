package weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.router;

public interface AdditionalInfoRouter {

    void showAdditionalInfoScreen(String cityName);
}
