package weatherapp.internship.five.agency.internshipweatherapp.domain.usecase;

import io.reactivex.Observable;
import weatherapp.internship.five.agency.internshipweatherapp.domain.model.Weather;
import weatherapp.internship.five.agency.internshipweatherapp.domain.repository.WeatherRepository;

public class GetWeatherForCityUseCaseImpl implements GetWeatherForCityUseCase {

    private final WeatherRepository weatherRepository;

    public GetWeatherForCityUseCaseImpl(final WeatherRepository weatherRepository) {
        this.weatherRepository = weatherRepository;
    }

    @Override
    public Observable<Weather> execute(final String city) {
        return weatherRepository.getWeatherForCity(city);
    }
}
