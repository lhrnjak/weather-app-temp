package weatherapp.internship.five.agency.internshipweatherapp.data.api.client;

import io.reactivex.Single;
import weatherapp.internship.five.agency.internshipweatherapp.data.api.WeatherRetrofitApi;
import weatherapp.internship.five.agency.internshipweatherapp.data.api.converter.ApiConverter;
import weatherapp.internship.five.agency.internshipweatherapp.domain.model.Weather;

public class WeatherRetrofitClientImpl implements WeatherRetrofitClient {

    private final ApiConverter apiConverter;
    private final WeatherRetrofitApi weatherRetrofitApi;
    private final String apiKey;

    public WeatherRetrofitClientImpl(final ApiConverter apiConverter, final WeatherRetrofitApi weatherRetrofitApi, final String apiKey) {
        this.apiConverter = apiConverter;
        this.weatherRetrofitApi = weatherRetrofitApi;
        this.apiKey = apiKey;
    }

    @Override
    public Single<Weather> getWeather(final String query, final String units) {
        return weatherRetrofitApi.getWeather(query, units, apiKey)
                                 .map(apiConverter::convertToWeather);
    }
}
