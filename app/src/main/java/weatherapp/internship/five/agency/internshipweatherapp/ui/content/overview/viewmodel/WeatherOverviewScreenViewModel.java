package weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.viewmodel;

import java.util.List;

public final class WeatherOverviewScreenViewModel {

    public final List<WeatherOverviewViewModel> weatherOverviewViewModels;

    public WeatherOverviewScreenViewModel(
            final List<WeatherOverviewViewModel> weatherOverviewViewModels) {
        this.weatherOverviewViewModels = weatherOverviewViewModels;
    }
}
