package weatherapp.internship.five.agency.internshipweatherapp.data.api.models;

import com.google.gson.annotations.SerializedName;

public class ApiWeatherCondition {

    @SerializedName("id")
    public int id;

    @SerializedName("main")
    public String mainParameters;

    @SerializedName("description")
    public String description;

    @SerializedName("icon")
    public String icon;
}
