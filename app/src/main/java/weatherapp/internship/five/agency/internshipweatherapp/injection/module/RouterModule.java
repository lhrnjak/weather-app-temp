package weatherapp.internship.five.agency.internshipweatherapp.injection.module;

import android.app.Activity;
import android.content.res.Resources;
import android.support.v4.app.FragmentManager;

import dagger.Module;
import dagger.Provides;
import weatherapp.internship.five.agency.internshipweatherapp.injection.scope.ActivityScope;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.router.AdditionalInfoRouter;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.router.AdditionalInfoRouterImpl;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.router.WeatherOverviewRouter;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.router.WeatherOverviewRouterImpl;

@Module
public final class RouterModule {

    @Provides
    @ActivityScope
    public WeatherOverviewRouter provideWeatherOverviewRouter(final Activity activity, final FragmentManager fragmentManager, final Resources resources) {
        return new WeatherOverviewRouterImpl(activity, fragmentManager, resources);
    }

    @Provides
    @ActivityScope
    public AdditionalInfoRouter provideAdditionalInfoRouter(final Activity activity, final FragmentManager fragmentManager) {
        return new AdditionalInfoRouterImpl(activity, fragmentManager);
    }

    public interface Exposes {

        WeatherOverviewRouter weatherOverviewRouter();

        AdditionalInfoRouter additionalInfoRouter();
    }
}
