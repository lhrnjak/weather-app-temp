package weatherapp.internship.five.agency.internshipweatherapp.data.api.converter;

import java.util.ArrayList;
import java.util.List;

import weatherapp.internship.five.agency.internshipweatherapp.data.api.models.ApiWeatherClouds;
import weatherapp.internship.five.agency.internshipweatherapp.data.api.models.ApiWeatherCondition;
import weatherapp.internship.five.agency.internshipweatherapp.data.api.models.ApiWeatherMain;
import weatherapp.internship.five.agency.internshipweatherapp.data.api.models.ApiWeatherResponse;
import weatherapp.internship.five.agency.internshipweatherapp.data.api.models.ApiWeatherWind;
import weatherapp.internship.five.agency.internshipweatherapp.domain.model.Weather;
import weatherapp.internship.five.agency.internshipweatherapp.domain.model.WeatherCondition;
import weatherapp.internship.five.agency.internshipweatherapp.util.StringUtils;

public class ApiConverterImpl implements ApiConverter {

    private static final String EMPTY = "";

    private final StringUtils stringUtils;

    public ApiConverterImpl(final StringUtils stringUtils) {
        this.stringUtils = stringUtils;
    }

    @Override
    public Weather convertToWeather(final ApiWeatherResponse apiWeatherResponse) {
        if (apiWeatherResponse == null) {
            return Weather.EMPTY;
        }
        if (apiWeatherResponse.main == null) {
            apiWeatherResponse.main = new ApiWeatherMain();
        }
        if (apiWeatherResponse.wind == null) {
            apiWeatherResponse.wind = new ApiWeatherWind();
        }
        if (apiWeatherResponse.clouds == null) {
            apiWeatherResponse.clouds = new ApiWeatherClouds();
        }
        final String city = itOrDefault(apiWeatherResponse.city, EMPTY);
        final double temperature = apiWeatherResponse.main.temperature;
        final int pressure = apiWeatherResponse.main.pressure;
        final int humidity = apiWeatherResponse.main.humidity;
        final int minimumTemperature = apiWeatherResponse.main.tempMin;
        final int maximumTemperature = apiWeatherResponse.main.tempMax;
        final int visibility = apiWeatherResponse.visibility;
        final double windSpeed = apiWeatherResponse.wind.speed;
        final int windDirection = apiWeatherResponse.wind.direction;
        final int cloudCoverage = apiWeatherResponse.clouds.cloudCover;

        final List<WeatherCondition> weatherConditions = convertToWeatherConditionList(apiWeatherResponse.weatherConditions);

        return new Weather(city, weatherConditions, temperature, pressure, humidity, minimumTemperature, maximumTemperature, visibility, windSpeed, windDirection, cloudCoverage);
    }

    private List<WeatherCondition> convertToWeatherConditionList(final List<ApiWeatherCondition> apiWeatherConditions) {
        final ArrayList<WeatherCondition> weatherCondition = new ArrayList<>();
        for (ApiWeatherCondition apiWeatherCondition : apiWeatherConditions) {
            weatherCondition.add(convertToWeatherCondition(apiWeatherCondition));
        }
        return weatherCondition;
    }

    private WeatherCondition convertToWeatherCondition(final ApiWeatherCondition apiWeatherCondition) {
        final int id = apiWeatherCondition.id;
        final String mainParameters = itOrDefault(apiWeatherCondition.mainParameters, EMPTY);
        final String description = itOrDefault(apiWeatherCondition.description, EMPTY);
        final String icon = itOrDefault(apiWeatherCondition.icon, EMPTY);

        return new WeatherCondition(id, mainParameters, description, icon);
    }

    private String itOrDefault(final String text, final String defaultText) {
        return stringUtils.itOrDefault(text, defaultText);
    }
}

