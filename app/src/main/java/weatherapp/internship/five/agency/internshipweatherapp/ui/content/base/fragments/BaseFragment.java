package weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import javax.inject.Inject;

import butterknife.ButterKnife;
import weatherapp.internship.five.agency.internshipweatherapp.injection.component.ActivityComponent;
import weatherapp.internship.five.agency.internshipweatherapp.injection.qualifier.ForActivity;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.activity.BaseActivity;

public abstract class BaseFragment extends Fragment {

    @Inject
    @ForActivity
    protected Context context;

    @Inject
    protected Resources resources;

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectMe();
    }

    protected View bindViews(final View view) {
        ButterKnife.bind(this, view);

        return view;
    }

    protected void injectMe() {
        inject(BaseActivity.from(getActivity()).getActivityComponent());
    }

    protected void inject(final ActivityComponent activityComponent) {

    }
}
