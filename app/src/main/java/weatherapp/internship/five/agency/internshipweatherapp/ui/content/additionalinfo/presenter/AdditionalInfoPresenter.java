package weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.presenter;

import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.view.AdditionalInfoView;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.Scoped;

public interface AdditionalInfoPresenter extends Scoped {

    void setView(AdditionalInfoView view);

    void setCityName(final String cityName);
}
