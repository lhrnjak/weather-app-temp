package weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import javax.inject.Inject;

import butterknife.ButterKnife;
import weatherapp.internship.five.agency.internshipweatherapp.R;
import weatherapp.internship.five.agency.internshipweatherapp.injection.component.ActivityComponent;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.activity.BaseActivity;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.router.WeatherOverviewRouter;

public class WeatherOverviewActivity extends BaseActivity {

    @Inject
    WeatherOverviewRouter router;

    public static Intent createIntent(final Context context) {
        return new Intent(context, WeatherOverviewActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_overview);

        bindViews();

        if (savedInstanceState == null) {
            router.showWeatherOverviewScreen();
        }
    }

    private void bindViews() {
        ButterKnife.bind(this);
    }

    @Override
    protected void inject(final ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }
}
