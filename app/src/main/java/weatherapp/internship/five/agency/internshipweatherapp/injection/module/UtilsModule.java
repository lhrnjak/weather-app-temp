package weatherapp.internship.five.agency.internshipweatherapp.injection.module;

import android.content.Context;

import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import weatherapp.internship.five.agency.internshipweatherapp.injection.qualifier.ForApplication;
import weatherapp.internship.five.agency.internshipweatherapp.util.ImageLoader;
import weatherapp.internship.five.agency.internshipweatherapp.util.ImageLoaderImpl;
import weatherapp.internship.five.agency.internshipweatherapp.util.ImageUrlBuilder;
import weatherapp.internship.five.agency.internshipweatherapp.util.ImageUrlBuilderImpl;
import weatherapp.internship.five.agency.internshipweatherapp.util.StringUtils;
import weatherapp.internship.five.agency.internshipweatherapp.util.StringUtilsImpl;
import weatherapp.internship.five.agency.internshipweatherapp.util.UriUtils;
import weatherapp.internship.five.agency.internshipweatherapp.util.UriUtilsImpl;

@Module
public final class UtilsModule {

    @Provides
    @Singleton
    StringUtils provideStringUtils() {
        return new StringUtilsImpl();
    }

    @Provides
    @Singleton
    UriUtils provideUriUtils(final StringUtils stringUtils) {
        return new UriUtilsImpl(stringUtils);
    }

    @Provides
    @Singleton
    Picasso providePicasso(@ForApplication final Context context) {
        return new Picasso.Builder(context).build();
    }

    @Provides
    @Singleton
    ImageLoader provideImageLoader(final Picasso picasso, final UriUtils uriUtils) {
        return new ImageLoaderImpl(picasso, uriUtils);
    }

    @Provides
    @Singleton
    ImageUrlBuilder provideImageUrlBuilder() {
        return new ImageUrlBuilderImpl();
    }

    public interface Exposes {

        StringUtils stringUtils();

        UriUtils uriUtils();

        Picasso picasso();

        ImageLoader imageLoader();

        ImageUrlBuilder imageUrlBuilder();
    }
}
