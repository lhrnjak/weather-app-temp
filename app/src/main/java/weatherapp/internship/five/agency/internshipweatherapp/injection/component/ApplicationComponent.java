package weatherapp.internship.five.agency.internshipweatherapp.injection.component;

import javax.inject.Singleton;

import dagger.Component;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.ApiModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.ApplicationModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.DataModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.ThreadingModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.UseCaseModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.UtilsModule;

@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                ApiModule.class,
                DataModule.class,
                UtilsModule.class,
                UseCaseModule.class,
                ThreadingModule.class
        }
)

public interface ApplicationComponent extends ApplicationComponentInjects,
                                              ApplicationComponentExposes { }
