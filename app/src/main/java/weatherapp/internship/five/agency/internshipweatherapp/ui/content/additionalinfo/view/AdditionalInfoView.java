package weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.view;

import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.viewmodel.AdditionalInfoViewModel;

public interface AdditionalInfoView {

    AdditionalInfoView EMPTY = new EmptyAdditionalInfoView();

    void renderView(AdditionalInfoViewModel viewModel);
}
