package weatherapp.internship.five.agency.internshipweatherapp.data.api.models;

import com.google.gson.annotations.SerializedName;

public class ApiWeatherMain {

    @SerializedName("temp")
    public double temperature;

    @SerializedName("pressure")
    public int pressure;

    @SerializedName("humidity")
    public int humidity;

    @SerializedName("temp_min")
    public int tempMin;

    @SerializedName("temp_max")
    public int tempMax;
}
