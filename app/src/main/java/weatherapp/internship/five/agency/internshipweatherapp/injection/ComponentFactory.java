package weatherapp.internship.five.agency.internshipweatherapp.injection;

import android.support.v7.app.AppCompatActivity;

import weatherapp.internship.five.agency.internshipweatherapp.application.WeatherApplication;
import weatherapp.internship.five.agency.internshipweatherapp.injection.component.ActivityComponent;
import weatherapp.internship.five.agency.internshipweatherapp.injection.component.ApplicationComponent;
import weatherapp.internship.five.agency.internshipweatherapp.injection.component.DaggerActivityComponent;
import weatherapp.internship.five.agency.internshipweatherapp.injection.component.DaggerApplicationComponent;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.ActivityModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.ApiModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.ApplicationModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.DataModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.PresenterModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.RouterModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.ThreadingModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.UseCaseModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.UtilsModule;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.activity.BaseActivity;

public class ComponentFactory {

    private ComponentFactory() {

    }

    public static ApplicationComponent createApplicationComponent(final WeatherApplication weatherApplication) {
        return DaggerApplicationComponent.builder()
                                         .applicationModule(new ApplicationModule(weatherApplication))
                                         .apiModule(new ApiModule())
                                         .dataModule(new DataModule())
                                         .threadingModule(new ThreadingModule())
                                         .utilsModule(new UtilsModule())
                                         .useCaseModule(new UseCaseModule())
                                         .build();
    }

    public static ActivityComponent createActivityComponent(final BaseActivity activity) {
        return createBase(activity).activityModule(new ActivityModule(activity))
                                   .presenterModule(new PresenterModule(activity))
                                   .routerModule(new RouterModule())
                                   .build();
    }

    public static DaggerActivityComponent.Builder createBase(final AppCompatActivity activity) {
        return DaggerActivityComponent.builder().applicationComponent(getApplicationComponent(activity));
    }

    public static ApplicationComponent getApplicationComponent(final AppCompatActivity appCompatActivity) {
        return WeatherApplication.from(appCompatActivity).getApplicationComponent();
    }
}
