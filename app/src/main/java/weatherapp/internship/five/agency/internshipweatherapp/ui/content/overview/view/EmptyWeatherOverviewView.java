package weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.view;

import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.viewmodel.WeatherOverviewScreenViewModel;

public final class EmptyWeatherOverviewView implements WeatherOverviewView {

    @Override
    public void renderView(final WeatherOverviewScreenViewModel viewModel) {
        // NO OP
    }
}
