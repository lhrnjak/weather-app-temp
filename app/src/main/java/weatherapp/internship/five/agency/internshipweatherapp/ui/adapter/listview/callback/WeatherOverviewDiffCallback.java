package weatherapp.internship.five.agency.internshipweatherapp.ui.adapter.listview.callback;

import android.support.v7.util.DiffUtil;

import java.util.List;

import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.viewmodel.WeatherOverviewViewModel;

public class WeatherOverviewDiffCallback extends DiffUtil.Callback {

    private final List<WeatherOverviewViewModel> oldData;
    private final List<WeatherOverviewViewModel> newData;

    public WeatherOverviewDiffCallback(final List<WeatherOverviewViewModel> oldData,
                                       final List<WeatherOverviewViewModel> newData) {
        this.oldData = oldData;
        this.newData = newData;
    }

    @Override
    public int getOldListSize() {
        return oldData.size();
    }

    @Override
    public int getNewListSize() {
        return newData.size();
    }

    @Override
    public boolean areItemsTheSame(final int oldItemPosition, final int newItemPosition) {
        final String oldCityName = oldData.get(oldItemPosition).cityName;
        final String newCityName = newData.get(newItemPosition).cityName;
        return oldCityName.equals(newCityName);
    }

    @Override
    public boolean areContentsTheSame(final int oldItemPosition, final int newItemPosition) {
        return oldData.get(oldItemPosition).equals(newData.get(newItemPosition));
    }
}
