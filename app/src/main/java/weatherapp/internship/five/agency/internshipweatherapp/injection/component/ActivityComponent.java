package weatherapp.internship.five.agency.internshipweatherapp.injection.component;

import dagger.Component;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.ActivityModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.PresenterModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.RouterModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.scope.ActivityScope;

@ActivityScope
@Component(
        dependencies = {
                ApplicationComponent.class
        },
        modules = {
                ActivityModule.class,
                PresenterModule.class,
                RouterModule.class
        }
)

public interface ActivityComponent extends ActivityComponentActivityInjections,
                                           ActivityComponentFragmentInjections,
                                           ActivityComponentViewInjections,
                                           ActivityComponentExposes { }
