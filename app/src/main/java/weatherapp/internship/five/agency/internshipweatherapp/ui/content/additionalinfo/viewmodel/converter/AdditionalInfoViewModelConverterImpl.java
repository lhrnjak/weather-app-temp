package weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.viewmodel.converter;

import weatherapp.internship.five.agency.internshipweatherapp.domain.model.Weather;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.viewmodel.AdditionalInfoViewModel;
import weatherapp.internship.five.agency.internshipweatherapp.util.ImageUrlBuilder;

public class AdditionalInfoViewModelConverterImpl implements AdditionalInfoViewModelConverter {

    private final ImageUrlBuilder imageUrlBuilder;

    public AdditionalInfoViewModelConverterImpl(final ImageUrlBuilder imageUrlBuilder) {
        this.imageUrlBuilder = imageUrlBuilder;
    }

    @Override
    public AdditionalInfoViewModel convertToAdditionalInfoViewModel(final Weather weather) {
        final String iconUrl = imageUrlBuilder.generateWeatherIconUrl(weather.weatherCondition.get(0).icon);

        return new AdditionalInfoViewModel(weather.city, weather.weatherCondition.get(0).description,
                                           iconUrl,
                                           weather.temperature, weather.minimumTemperature, weather.maximumTemperature,
                                           weather.pressure, weather.humidity,
                                           weather.visibility,
                                           weather.windSpeed, weather.windDirection,
                                           weather.cloudCover);
    }
}
