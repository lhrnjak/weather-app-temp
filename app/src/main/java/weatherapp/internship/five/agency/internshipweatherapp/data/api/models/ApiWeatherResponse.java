package weatherapp.internship.five.agency.internshipweatherapp.data.api.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiWeatherResponse {

    @SerializedName("weather")
    public List<ApiWeatherCondition> weatherConditions;

    @SerializedName("main")
    public ApiWeatherMain main;

    @SerializedName("visibility")
    public int visibility;

    @SerializedName("wind")
    public ApiWeatherWind wind;

    @SerializedName("clouds")
    public ApiWeatherClouds clouds;

    @SerializedName("name")
    public String city;

    @SerializedName("cod")
    public int responseCode;
}
