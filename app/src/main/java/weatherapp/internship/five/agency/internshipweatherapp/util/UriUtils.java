package weatherapp.internship.five.agency.internshipweatherapp.util;

import android.net.Uri;

public interface UriUtils {

    Uri toUriOrDefault(String uriAsString);
}
