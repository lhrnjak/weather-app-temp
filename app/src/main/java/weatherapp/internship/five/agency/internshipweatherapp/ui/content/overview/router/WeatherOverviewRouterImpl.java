package weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.router;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import weatherapp.internship.five.agency.internshipweatherapp.R;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.activity.AdditionalInfoActivity;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.additionalinfo.fragments.AdditionalInfoFragment;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.fragments.WeatherOverviewFragment;

public class WeatherOverviewRouterImpl implements WeatherOverviewRouter {

    private static final String FRAGMENT_WEATHER_OVERVIEW_FRAGMENT_TAG = "weather_overview_fragment";
    private static final String FRAGMENT_ADDITIONAL_INFO_FRAGMENT_TAG = "additional_info_fragment";

    private static final int PRIMARY_FRAGMENT_CONTAINER = R.id.activity_weather_overview_container;
    private static final int ADDITIONAL_INFO_LANDSCAPE_CONTAINER = R.id.activity_weather_overview_additional_info_container;

    private final Activity activity;
    private final FragmentManager fragmentManager;
    private final Resources resources;

    public WeatherOverviewRouterImpl(final Activity activity, final FragmentManager fragmentManager, final Resources resources) {
        this.activity = activity;
        this.fragmentManager = fragmentManager;
        this.resources = resources;
    }

    @Override
    public void showWeatherOverviewScreen() {
        addFragmentToUi(WeatherOverviewFragment.newInstance(), PRIMARY_FRAGMENT_CONTAINER, FRAGMENT_WEATHER_OVERVIEW_FRAGMENT_TAG);
    }

    @Override
    public void showAdditionalInfoScreen(String cityName) {
        if (isTabletLandscape()) {
            addFragmentToUi(AdditionalInfoFragment.newInstance(cityName), ADDITIONAL_INFO_LANDSCAPE_CONTAINER, FRAGMENT_ADDITIONAL_INFO_FRAGMENT_TAG);
        } else {
            activity.startActivity(AdditionalInfoActivity.createIntent(activity).putExtra(Intent.EXTRA_TEXT, cityName));
        }
    }

    private void addFragmentToUi(final Fragment fragment, final int containerId, final String tag) {
        fragmentManager.beginTransaction()
                       .replace(containerId, fragment, tag)
                       .commitAllowingStateLoss();
    }

    private boolean isTabletLandscape() {
        return resources.getBoolean(R.bool.tablet_landscape_mode);
    }
}
