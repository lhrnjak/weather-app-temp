package weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import weatherapp.internship.five.agency.internshipweatherapp.injection.ComponentFactory;
import weatherapp.internship.five.agency.internshipweatherapp.injection.component.ActivityComponent;

public abstract class BaseActivity extends AppCompatActivity {

    private ActivityComponent activityComponent;

    public static BaseActivity from(final Context context) {
        if (!(context instanceof BaseActivity)) {
            throw new IllegalArgumentException("Context must be instance of BaseActivity");
        }
        return ((BaseActivity) context);
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActivityComponent();
        inject(activityComponent);
    }

    private void initActivityComponent() {
        activityComponent = createActivityComponent();
    }

    public final ActivityComponent getActivityComponent() {
        if (activityComponent == null) {
            initActivityComponent();
        }
        return activityComponent;
    }

    protected ActivityComponent createActivityComponent() {
        return ComponentFactory.createActivityComponent(this);
    }

    protected void inject(final ActivityComponent activityComponent) {

    }
}
