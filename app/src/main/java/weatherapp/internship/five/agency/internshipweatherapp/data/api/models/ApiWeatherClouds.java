package weatherapp.internship.five.agency.internshipweatherapp.data.api.models;

import com.google.gson.annotations.SerializedName;

public class ApiWeatherClouds {

    @SerializedName("all")
    public int cloudCover;
}
