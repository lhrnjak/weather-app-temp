package weatherapp.internship.five.agency.internshipweatherapp.util;

public class ImageUrlBuilderImpl implements ImageUrlBuilder {

    @Override
    public String generateWeatherIconUrl(final String iconId) {
        return OPEN_WEATHER_MAP_ICON_BASIC_URL + iconId + ".png";
    }
}
