package weatherapp.internship.five.agency.internshipweatherapp.injection.component;

import weatherapp.internship.five.agency.internshipweatherapp.injection.module.ApiModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.ApplicationModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.DataModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.ThreadingModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.UseCaseModule;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.UtilsModule;

public interface ApplicationComponentExposes extends ApplicationModule.Exposes,
                                                     ApiModule.Exposes,
                                                     DataModule.Exposes,
                                                     UtilsModule.Exposes,
                                                     UseCaseModule.Exposes,
                                                     ThreadingModule.Exposes {
}
