package weatherapp.internship.five.agency.internshipweatherapp.domain.model;

import java.util.ArrayList;
import java.util.List;

public class Weather {

    public static final Weather EMPTY = new Weather("", new ArrayList<>(), 0.0, 0, 0, 0, 0, 0, 0.0, 0, 0);

    public final String city;
    public final List<WeatherCondition> weatherCondition;
    public final double temperature;
    public final int pressure;
    public final int humidity;
    public final int minimumTemperature;
    public final int maximumTemperature;
    public final int visibility;
    public final double windSpeed;
    public final int windDirection;
    public final int cloudCover;

    public Weather(final String city, final List<WeatherCondition> weatherCondition, final double temperature, final int pressure, final int humidity, final int minimumTemperature,
                   final int maximumTemperature, final int visibility,
                   final double windSpeed, final int windDirection, final int cloudCover) {
        this.city = city;
        this.weatherCondition = weatherCondition;
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.minimumTemperature = minimumTemperature;
        this.maximumTemperature = maximumTemperature;
        this.visibility = visibility;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
        this.cloudCover = cloudCover;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Weather weather = (Weather) o;

        if (Double.compare(weather.temperature, temperature) != 0) {
            return false;
        }
        if (pressure != weather.pressure) {
            return false;
        }
        if (humidity != weather.humidity) {
            return false;
        }
        if (minimumTemperature != weather.minimumTemperature) {
            return false;
        }
        if (maximumTemperature != weather.maximumTemperature) {
            return false;
        }
        if (visibility != weather.visibility) {
            return false;
        }
        if (Double.compare(weather.windSpeed, windSpeed) != 0) {
            return false;
        }
        if (windDirection != weather.windDirection) {
            return false;
        }
        if (cloudCover != weather.cloudCover) {
            return false;
        }
        if (city != null ? !city.equals(weather.city) : weather.city != null) {
            return false;
        }
        return weatherCondition != null ? weatherCondition.equals(weather.weatherCondition) : weather.weatherCondition == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = city != null ? city.hashCode() : 0;
        result = 31 * result + (weatherCondition != null ? weatherCondition.hashCode() : 0);
        temp = Double.doubleToLongBits(temperature);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + pressure;
        result = 31 * result + humidity;
        result = 31 * result + minimumTemperature;
        result = 31 * result + maximumTemperature;
        result = 31 * result + visibility;
        temp = Double.doubleToLongBits(windSpeed);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + windDirection;
        result = 31 * result + cloudCover;
        return result;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "city='" + city + '\'' +
                ", weatherCondition=" + weatherCondition +
                ", temperature=" + temperature +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", minimumTemperature=" + minimumTemperature +
                ", maximumTemperature=" + maximumTemperature +
                ", visibility=" + visibility +
                ", windSpeed=" + windSpeed +
                ", windDirection=" + windDirection +
                ", cloudCover=" + cloudCover +
                '}';
    }
}
