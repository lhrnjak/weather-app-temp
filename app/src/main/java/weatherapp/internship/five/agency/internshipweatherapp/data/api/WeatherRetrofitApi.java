package weatherapp.internship.five.agency.internshipweatherapp.data.api;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;
import weatherapp.internship.five.agency.internshipweatherapp.data.api.models.ApiWeatherResponse;

public interface WeatherRetrofitApi {

    String API_URL = "http://api.openweathermap.org/data/2.5/";
    String UNITS_METRIC = "metric";
    String UNITS_IMPERIAL = "imperial";

    @GET(WeatherRetrofitApiConstants.PATH_CURRENT_WEATHER)
    Single<ApiWeatherResponse> getWeather(@Query(WeatherRetrofitApiConstants.PARAM_QUERY) String query,
                                          @Query(WeatherRetrofitApiConstants.PARAM_UNITS) String units,
                                          @Query(WeatherRetrofitApiConstants.PARAM_APPID) String appid);
}
