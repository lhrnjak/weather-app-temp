package weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.presenter;

import android.support.annotation.NonNull;

import java.lang.ref.WeakReference;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import weatherapp.internship.five.agency.internshipweatherapp.injection.module.ThreadingModule;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.Scoped;
import weatherapp.internship.five.agency.internshipweatherapp.util.ObjectUtils;

public abstract class BasePresenter<T> implements Scoped {

    private final T emptyView;

    @Inject
    @Named(ThreadingModule.SUBSCRIBE_SCHEDULER)
    protected Scheduler subscribeScheduler;

    @Inject
    @Named(ThreadingModule.OBSERVE_SCHEDULER)
    protected Scheduler observeScheduler;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private WeakReference<T> viewWeakReference;

    public BasePresenter(@NonNull final T emptyView) {
        this.emptyView = emptyView;
    }

    @Override
    public void activate() {
        removeDisposables();
        initDisposable();
    }

    private void initDisposable() {
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void deactivate() {
        removeDisposables();
    }

    protected final void setViewInternal(T view) {
        this.viewWeakReference = new WeakReference<>(view);
    }

    @NonNull
    protected final T getView() {
        if (viewWeakReference == null) {
            return emptyView;
        }

        return ObjectUtils.itOrDefault(viewWeakReference.get(), emptyView);
    }

    private void removeDisposables() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    protected final void addDisposable(final Disposable disposable) {
        compositeDisposable.add(disposable);
    }
}
