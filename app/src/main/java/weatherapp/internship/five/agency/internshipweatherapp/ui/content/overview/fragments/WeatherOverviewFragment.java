package weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.BindView;
import weatherapp.internship.five.agency.internshipweatherapp.R;
import weatherapp.internship.five.agency.internshipweatherapp.injection.component.ActivityComponent;
import weatherapp.internship.five.agency.internshipweatherapp.ui.adapter.listview.WeatherOverviewAdapter;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.base.fragments.BaseFragment;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.presenter.WeatherOverviewPresenter;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.view.WeatherOverviewView;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.viewmodel.WeatherOverviewScreenViewModel;

public class WeatherOverviewFragment extends BaseFragment implements WeatherOverviewView, WeatherOverviewAdapter.Listener {

    @BindView(R.id.fragment_weather_overview_recycler_view)
    protected RecyclerView recyclerView;

    private LinearLayoutManager layoutManager;

    @Inject
    WeatherOverviewPresenter presenter;

    @Inject
    WeatherOverviewAdapter adapter;

    public static Fragment newInstance() {
        return new WeatherOverviewFragment();
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return bindViews(inflater.inflate(R.layout.fragment_weather_overview, container, false));
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initRecyclerView();
        initAdapter();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.setView(this);
        presenter.activate();
    }

    @Override
    public void onPause() {
        presenter.deactivate();
        super.onPause();
    }

    @Override
    public void citySelected(final String cityName) {
        presenter.showAdditionalInfo(cityName);
    }

    private void initRecyclerView() {
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(false);
        recyclerView.setAdapter(adapter);
    }

    private void initAdapter() {
        adapter.setListener(this);
    }

    @Override
    public void renderView(final WeatherOverviewScreenViewModel viewModel) {
        adapter.setWeatherData(viewModel.weatherOverviewViewModels);
    }

    @Override
    public void inject(final ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }
}
