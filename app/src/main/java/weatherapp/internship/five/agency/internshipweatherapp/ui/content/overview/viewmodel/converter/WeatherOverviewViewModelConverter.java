package weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.viewmodel.converter;

import weatherapp.internship.five.agency.internshipweatherapp.domain.model.Weather;
import weatherapp.internship.five.agency.internshipweatherapp.ui.content.overview.viewmodel.WeatherOverviewViewModel;

public interface WeatherOverviewViewModelConverter {

    WeatherOverviewViewModel convertToViewModel(Weather weather);
}
