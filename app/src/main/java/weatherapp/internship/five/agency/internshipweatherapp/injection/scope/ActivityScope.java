package weatherapp.internship.five.agency.internshipweatherapp.injection.scope;

import javax.inject.Scope;

@Scope
public @interface ActivityScope { }
