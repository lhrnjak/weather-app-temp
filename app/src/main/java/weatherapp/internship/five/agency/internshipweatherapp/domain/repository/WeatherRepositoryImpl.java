package weatherapp.internship.five.agency.internshipweatherapp.domain.repository;

import io.reactivex.Observable;
import weatherapp.internship.five.agency.internshipweatherapp.data.api.WeatherRetrofitApi;
import weatherapp.internship.five.agency.internshipweatherapp.data.api.client.WeatherRetrofitClient;
import weatherapp.internship.five.agency.internshipweatherapp.domain.model.Weather;

public class WeatherRepositoryImpl implements WeatherRepository {

    private final WeatherRetrofitClient weatherRetrofitClient;

    public WeatherRepositoryImpl(final WeatherRetrofitClient weatherRetrofitClient) {
        this.weatherRetrofitClient = weatherRetrofitClient;
    }

    @Override
    public Observable<Weather> getWeatherForCity(final String city) {
        return weatherRetrofitClient.getWeather(city, WeatherRetrofitApi.UNITS_METRIC)
                                    .toObservable();
    }
}
