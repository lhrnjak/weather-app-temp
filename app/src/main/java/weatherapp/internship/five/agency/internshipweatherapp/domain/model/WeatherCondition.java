package weatherapp.internship.five.agency.internshipweatherapp.domain.model;

public class WeatherCondition {

    public final int id;
    public final String mainParameters;
    public final String description;
    public final String icon;

    public WeatherCondition(final int id, final String mainParameters, final String description, final String icon) {
        this.id = id;
        this.mainParameters = mainParameters;
        this.description = description;
        this.icon = icon;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final WeatherCondition that = (WeatherCondition) o;

        if (id != that.id) {
            return false;
        }
        if (mainParameters != null ? !mainParameters.equals(that.mainParameters) : that.mainParameters != null) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        return icon != null ? icon.equals(that.icon) : that.icon == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (mainParameters != null ? mainParameters.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "WeatherCondition{" +
                "id=" + id +
                ", mainParameters='" + mainParameters + '\'' +
                ", description='" + description + '\'' +
                ", icon='" + icon + '\'' +
                '}';
    }
}
