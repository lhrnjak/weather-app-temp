package weatherapp.internship.five.agency.internshipweatherapp.injection.qualifier;

import javax.inject.Qualifier;

@Qualifier
public @interface ForActivity {
}
