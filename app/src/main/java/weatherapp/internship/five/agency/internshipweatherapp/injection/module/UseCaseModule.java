package weatherapp.internship.five.agency.internshipweatherapp.injection.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import weatherapp.internship.five.agency.internshipweatherapp.domain.repository.WeatherRepository;
import weatherapp.internship.five.agency.internshipweatherapp.domain.usecase.GetWeatherForCityUseCase;
import weatherapp.internship.five.agency.internshipweatherapp.domain.usecase.GetWeatherForCityUseCaseImpl;

@Module
public final class UseCaseModule {

    @Provides
    @Singleton
    public GetWeatherForCityUseCase provideGetWeatherUseCase(final WeatherRepository weatherRepository) {
        return new GetWeatherForCityUseCaseImpl(weatherRepository);
    }

    public interface Exposes {

        GetWeatherForCityUseCase getWeatherUseCase();
    }
}
